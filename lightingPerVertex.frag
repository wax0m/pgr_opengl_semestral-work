#version 140

struct Material {           // structure that describes currently used material
  vec3  ambient;            // ambient component
  vec3  diffuse;            // diffuse component
  vec3  specular;           // specular component
  float shininess;          // sharpness of specular reflection

  bool  useTexture;         // defines whether the texture is used or not
};

uniform sampler2D texSampler;  // sampler for the texture access

struct Light {         // structure describing light parameters
	vec3  ambient;       // intensity & color of the ambient component
	vec3  diffuse;       // intensity & color of the diffuse component
	vec3  specular;      // intensity & color of the specular component
  
	vec3  position;      // light position
	vec3  spotDirection; // spotlight direction
	float spotCosCutOff; // cosine of the spotlight's half angle
	float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)
						// also as outer cut off
	float innerCutOff;
	float outerCutOff;

	float constant;
	float linear;
	float quadratic;

	float intensity;
};


uniform Material material;     // current material


uniform float time;         // time used for simulation of moving lights (such as sun)


uniform mat4 PVMmatrix;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 Vmatrix;       // View                       --> world to eye coordinates
uniform mat4 Mmatrix;       // Model                      --> model to world coordinates
uniform mat4 normalMatrix;  // inverse transposed Mmatrix


uniform vec3 cameraPos;		// insert cameraPos
uniform bool inFirstPerson;	// first person

uniform vec3 reflectorPosition;   // reflector position (world coordinates)
uniform vec3 reflectorDirection;  // reflector direction (world coordinates)

uniform vec3 lampPosition;		  // lamp position (world coordinates)
uniform bool lampOn;

uniform bool fogOn;


smooth in vec2 texCoord_v;     // fragment texture coordinates

in vec3 vertexPosition;
in vec3 vertexNormal;


out vec4       color_f;        // outgoing fragment color


// hardcoded lights
Light sun;
float sunSpeed = 1.0f;
Light playerReflector;

Light playerPointLight;

Light mainChandelier;


float fogDistance(vec3 vertexPosition) {
	float fog = 0.0f;
	//float density = 0.8f;
	float dist = length(vertexPosition);
	float density = 0.8f;
	fog = clamp(exp(-pow(dist * density, 2)), 0, 1);

	return fog;
}


vec4 pointLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
	vec3 L = normalize(light.position - vertexPosition);			// Light direction
	vec3 R = reflect(-L, vertexNormal);								// Light reflection
	vec3 V;															// View direction
	//if (inFirstPerson)
	//	V = normalize(cameraPos- vertexPosition);				
	//else
		V = normalize(-vertexPosition);

	// soft edge
	float theta		= dot(L, normalize(-light.spotDirection));
	float epsilon   = (light.spotCosCutOff - light.spotExponent);
	float intensity = clamp((theta - light.spotExponent) / epsilon, 0.0, 1.0);
  	
	// attenuation
    float distance    = length(light.position - vertexPosition);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    

	// ambient
	vec3 ambient = light.ambient * material.ambient * texture(texSampler, texCoord_v).rgb;

    // diffuse 
	float diff	  = max(dot(L, vertexNormal), 0.0);
    vec3 diffuse  = light.diffuse * diff * material.diffuse * texture(texSampler, texCoord_v).rgb;  
    
    // specular
    float spec		= pow(max(dot(R, V), 0.0), material.shininess);
    vec3 specular	= light.specular * spec * material.specular * texture(texSampler, texCoord_v).rgb;  
    /*
	diffuse	 *= intensity;
	specular *= intensity;
    */

    ambient  *= attenuation;  
    diffuse  *= attenuation;
    specular *= attenuation;   
    
	
    vec3 ret = ambient + diffuse + specular;

	return vec4(ret, 1.0);
}


vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

  vec3 ret = vec3(0.0);

	vec3 L = normalize(light.position - vertexPosition);			// Light direction
	vec3 R = reflect(-L, vertexNormal);								// Light reflection
	vec3 V;															// View direction
	if (!inFirstPerson)
		V = normalize(normalize(cameraPos) - vertexPosition);				
	else
		V = normalize(-vertexPosition);

	float dotAngle = dot(-L, light.spotDirection);
	if (dotAngle > light.spotCosCutOff) {
		
		// attenuation
		float dist		  = length(light.position - vertexPosition);
		float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));
		
		// soft edges (SOMEHOW NOT WORKING)
		float theta		= dot(L, normalize(-light.spotDirection));
		float epsilon   = (light.spotCosCutOff - light.spotExponent);
		float intensity = clamp((theta - light.spotExponent) / epsilon, 0.0, 1.0);

		// ambient
		vec3 ambient  = light.ambient * material.ambient * texture(texSampler, texCoord_v).rgb;

		// diffuse
		vec3 norm	  = normalize(vertexNormal);
		vec3 diffuse  = light.diffuse * material.diffuse * max(dot(L, vertexNormal), 0.0) * texture(texSampler, texCoord_v).rgb;

		// specular
		vec3 specular = light.specular * material.specular * pow(max(dot(R, V), 0.0), material.shininess) * texture(texSampler, texCoord_v).rgb;
		
		diffuse	 *= intensity;
		specular *= intensity;
		

		ambient  *= attenuation;
		diffuse	 *= attenuation;
		specular *= attenuation;

		ret = ambient + diffuse + specular;
		
	}
	else {
		// attenuation
		float dist		  = length(light.position - vertexPosition);
		float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));

		// ambient
		vec3 globalAmbientLight = vec3(0.05f);
		vec3 ambient  = material.ambient * texture(texSampler, texCoord_v).rgb * globalAmbientLight;
		ret = ambient*attenuation;
	}

  return vec4(ret, 1.0);
}

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

	vec3 ret = vec3(0.0);

  
	float dist		  = length(light.position - vertexPosition);
	float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));
	vec3 V;															// View direction
	if (!inFirstPerson)
		V = normalize(normalize(cameraPos) - vertexPosition);				
	else
		V = normalize(normalize((Mmatrix * vec4(0.0f, 0.0f, 1.5f, 1.0f)).xyz) - vertexPosition);								// This is original

	vec3 norm = normalize(vertexNormal);

	vec3 L = normalize(-light.spotDirection);
	vec3 R = reflect(-L, norm);
	//vec3 V = normalize(cameraPos - vertexPosition);


	// ambient
	vec3 ambient  = light.ambient * material.ambient * texture(texSampler, texCoord_v).rgb;

	// diffuse
	float diff	  = max(dot(norm, L), 0.0);
	vec3 diffuse  = light.diffuse * material.diffuse * texture(texSampler, texCoord_v).rgb * diff;

	// specular
	float spec	  = pow(max(dot(V, R), 0.0), material.shininess);
	vec3 specular = light.specular * material.specular * texture(texSampler, texCoord_v).rgb * spec;
	
	ret = ambient + diffuse + specular;

	return vec4(ret, 1.0);
}

void setupLights() {


  // set up sun parameters
  sun.ambient  = vec3(0.0);
  sun.diffuse  = vec3(0.8, 0.8, 0.5f);
  sun.specular = vec3(1.0);
  float sunAngle = (time * sunSpeed);

  if (sunAngle > 360)
	sunAngle = 0;
  sun.intensity = sunAngle / 360;
  /*
  if (sunAngle >= 90)
	sunAngle = 0;
	*/
  sun.position = vec3(0.0, 0.0, 0.5);
  sun.spotDirection = sun.position;
  sun.spotDirection.z = -1.0f;
  //sun.position = (Vmatrix * vec4(cos(sunAngle), 0.0, sin(sunAngle), 0.0)).xyz;


  // set up reflector parameters
  playerReflector.ambient       = vec3(0.2f);
  playerReflector.diffuse       = vec3(1.0);
  playerReflector.specular      = vec3(1.0);
  playerReflector.spotCosCutOff = 0.91f;
  playerReflector.spotExponent  = 0.82f;

  playerReflector.constant		= 1.0f;
  playerReflector.linear		= 0.7f;
  playerReflector.quadratic		= 1.8f;


  playerReflector.position		= (Vmatrix * vec4(reflectorPosition, 1.0)).xyz;
  playerReflector.spotDirection = (Vmatrix * vec4(reflectorDirection, 0.0)).xyz;


  // set up player point light parameters
  playerPointLight.ambient       = vec3(0.2f);
  playerPointLight.diffuse       = vec3(1.0);
  playerPointLight.specular      = vec3(1.0);
  playerPointLight.spotCosCutOff = 0.91f;
  playerPointLight.spotExponent  = 0.82f;

  playerPointLight.constant		= 1.0f;
  playerPointLight.linear		= 0.7f;
  playerPointLight.quadratic	= 1.8f;


  playerPointLight.position		 = (vec4(reflectorPosition.x, reflectorPosition.y, reflectorPosition.z + 0.3f, 1.0)).xyz;
  playerPointLight.spotDirection = (vec4(reflectorDirection, 0.0)).xyz;


  // set up chandelier point light parameters
  mainChandelier.ambient       = vec3(0.2f);
  mainChandelier.diffuse       = vec3(1.0, 1.0, 0.3);
  mainChandelier.specular      = vec3(1.0);
  mainChandelier.spotCosCutOff = 0.91f;
  mainChandelier.spotExponent  = 0.82f;

  mainChandelier.constant		= 1.0f;
  mainChandelier.linear			= 0.35f;
  mainChandelier.quadratic		= 0.44f;


  mainChandelier.position		 = (vec4(lampPosition, 1.0)).xyz;
  mainChandelier.position.z		 = mainChandelier.position.z - 0.1f;
  mainChandelier.spotDirection	 = (vec4(lampPosition.x, lampPosition.y, -1.0, 0.0)).xyz;


}


void main() {
	setupLights();

	// initialize the output color with the global ambient term
	vec4 outputColor;
	vec3 globalAmbientLight = vec3(0.05f);
	outputColor = vec4(material.ambient * globalAmbientLight, 1.0);
	
	//outputColor += vec4(material.diffuse, 0);

	// accumulate contributions from all lights
	outputColor += directionalLight(sun, material, vertexPosition, vertexNormal);

	if (lampOn)
		outputColor += pointLight(mainChandelier, material, vertexPosition, vertexNormal);
	outputColor += spotLight(playerReflector, material, vertexPosition, vertexNormal);
	color_f = outputColor;

	float fog = fogDistance(vertexPosition);

	// if material has a texture -> apply it
	if(material.useTexture)
		color_f =  color_f * vec4(texture(texSampler, texCoord_v).rgb, 1.0);
	else 
		color_f += vec4(material.diffuse, 0);

	if (fogOn) 
		color_f = mix(vec4(0.8f, 0.8f, 0.8f, 1.0f), color_f, fog);
}
